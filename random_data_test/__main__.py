import sys
from random_modules import random_line_generator

text_lines_number = int(sys.argv[1])

if len(sys.argv) == 2 and type(text_lines_number) == int:        
    random_line_generator(text_lines_number)

