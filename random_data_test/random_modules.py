import random
import uuid
from datetime import datetime
import platform

# Función que nos proporciona una fecha aleatoria
def random_date():
    start = datetime(2021, 1, 1)
    final =  datetime(2021, 12, 31)

    random_date = start + (final - start) * random.random()
    
    return "{}".format(random_date)

# Función que nos proporciona modelo y número del ordenador de forma aleatoria
def random_model_name_number():
    manufacturer_name = ["Empire", "Nostromo", "Nexus", "Dune", "Foundation", "Jedi", "Neo", "Hall"]
    model_number = ["2000", "3000", "4000", "5000"]
    model = manufacturer_name[random.randint(0, 7)] + " " + model_number[random.randint(0, 3)]
    
    return model
    
#Función que nos proporciona ID ordenador en formato UUID4 de forma aleatoria
def random_id():
    id = uuid.uuid4()
    
    return "{}".format(id)

# Función que nos proporciona la temperatura aleatoria del ambiente y el ordenador
def random_temperature():
    machine_temperature = random.randint(40, 80)
    environment_temperature = random.randint(20, 40)
    
    return '{0},{1}'.format(machine_temperature,environment_temperature)
    
# Porcentajes de batería, señal, CPU y uso de RAM aleatorios
def random_percentages():
    batery = random.randint(3, 100)
    cpu = random.randint(0, 100)
    signal = random.randint(0, 100)
    ram = random.randint(0, 100)
  
    return '{0},{1},{2},{3}'.format(batery, cpu, signal,ram)
    
# Función que nos proporciona de forma aleatoria los estados del USB, Disco Duro y WIFI
def random_states():
    usb_state = ["TRUE", "FALSE"]
    disc_state = ["TRUE", "FALSE"]
    wifi_state = ["enabled", "disabled", "connected"]
    
    usb = usb_state[random.randint(0, 1)]
    disc = disc_state[random.randint(0, 1)]
    wifi = wifi_state[random.randint(0, 2)]
    
    return '{0},{1},{2}'.format(usb, disc, wifi)
    
# Función que nos proporciona de forma aleatoria longitud y latitud
def random_coordenate():
    longitude = random.uniform(40.504469856075815, 40.337191596410385)
    latitude = random.uniform(-3.593027469438119, -3.7056373309049535)
     
    return '{0},{1}'.format(longitude, latitude)
    
# Una función para concatenarlas a todas
def concatenate_random():
    date = random_date()
    model_name = random_model_name_number()
    id = random_id()
    temperature = random_temperature()
    percentages = random_percentages()
    status = random_states()
    coordenates = random_coordenate()
    line = '{0},{1},{2},{3},{4},{5},{6}'.format(date, model_name, id, temperature, 
    percentages, status, coordenates) 
    
    return line

# Imprime n lineas
def random_line_generator(lines):
    if platform.system == "Windows":
        with open(".\cluster_error_test.log", 'w') as file:
            for i in range(lines):
                file.write(concatenate_random() + "\n")
    else:
        with open("./cluster_error_test.log", 'w') as file:
            for i in range(lines):
                file.write(concatenate_random() + "\n")
        